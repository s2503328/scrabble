package scrabble.newProtocol;

import java.net.Socket;

public interface ClientInterface {
    /**
     *
     * @param socket
     * tries to create a connection between the server.
     */
    public void createConnection(Socket socket);

    /**
     *
     * @param txt
     * sends the message received from client's console.
     */
    void sendMessage(String txt);

    /**
     * listens for messages coming from server.
     */
    void listenForMessages();

}
