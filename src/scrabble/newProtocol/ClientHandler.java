package scrabble.newProtocol;

import scrabble.controller.Controller;
import scrabble.exceptions.InvalidDirectionException;
import scrabble.model.Player;
import scrabble.model.Tiles;
import scrabble.view.PrinterBoard;


import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientHandler implements Runnable {
    private Socket socket;
     BufferedReader in;
     public BufferedWriter out;
    public Player player;
    private String username;
    private Controller controller;
    private PrinterBoard view = new PrinterBoard();

    /**
     * @requires socket != null && controller != null
     * @ensures clientMap != null && BufferedReader in != null && BufferedWriter out != null
     * @param socket
     * @param controller
     * constructor of the class
     * creates clients and sets up bufferedreaders/writers, welcomes users and informs users others have joined
     */
    public ClientHandler(Socket socket, Controller controller) {
        this.socket = socket;
        this.controller = controller;


        try{
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            this.username = in.readLine();
            this.player = new Player(username);

            controller.clientMap.put(this, this.getPlayer());
            out.write("Welcome to the server " + username + "!");
            out.newLine();
            out.flush();
            broadcastJOINMessage("Player " + username + " has joined the server!");
            if (controller.clientMap.size() >= 2){
                broadcastMessageAll("There is enough players to play the game.\nType READY when you are ready");
            }


        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public Player getPlayer() {
        return player;
    }

    /**
     * starts when thread starts
     */
    @Override
    public void run() {
        String msg;
        try {
            msg = in.readLine();

            while (msg != null) {

                handleCommand(msg);

                if (controller.game != null){
                    if (controller.game.gameOver()){
                        controller.declareWinner();
                        break;
                    }
                }

                msg = in.readLine();
            }

            for (ClientHandler clientHandler: controller.clientMap.keySet()){
                clientHandler.shutdown();
            }
        } catch (IOException e) {
            shutdown();
        }

    }

    /**
     * @requires msg != null
     * @param msg
     * @throws IOException
     * sends the correct message to controller accordingly.
     */
    private void handleCommand(String msg) throws IOException {
        msg = msg.toUpperCase();

        switch (msg.split(ProtocolMessages.DELIMITER)[0]){
            case "MOVE":
                String word = msg.split(ProtocolMessages.DELIMITER)[1];
                String position = msg.split(ProtocolMessages.DELIMITER)[2];
                String direction = msg.split(ProtocolMessages.DELIMITER)[3];

                controller.doMove(word,position,direction,this);
                break;
            case "SWAP":
                ArrayList<Tiles> tiles = new ArrayList<>();
                String[] stringTiles = msg.split(ProtocolMessages.DELIMITER)[1].split("");
                for (String s : stringTiles){
                    tiles.add(Tiles.valueOf(s));
                }
                controller.doSwap(tiles,this);
                out.write("tiles swapped");
                out.newLine();
                out.flush();
                break;
            case "SKIP":
                controller.Skip();
                out.write(view.skipMessage());
                out.newLine();
                out.flush();
                break;
            case "CHAT":
                broadcastMessage(msg.substring(5));
                break;
            case "READY":
                controller.Ready();
                break;
            case "HELP":
                controller.doHelp(this);
            default:
                controller.doHelp(this);
                break;
        }

    }

    /**
     * @ensures BufferedReader in, BufferedWriter out and socket is closed.
     * shuts down if there is an error with server or client.
     */
    private void shutdown() {
        System.out.println("> [" + username + "] Shutting down.");
        try {
            removeClientHandler();
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * @requires message != null
     * @param message
     * send message to other clients.
     */
    public void broadcastMessage(String message){
        for (ClientHandler mcl: controller.clientMap.keySet()) {
            try {
                if (!mcl.username.equals(this.username)) {
                    mcl.out.write(username + ": " + message);
                    mcl.out.newLine();
                    mcl.out.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * @requires message != null
     * @param message
     * broadcasts a message to other clients when a player joins.
     */
    public void broadcastJOINMessage(String message){
        for (ClientHandler mcl: controller.clientMap.keySet()) {
            try {
                if (!mcl.username.equals(this.username)) {
                    mcl.out.write(  "SERVER : " + message);
                    mcl.out.newLine();
                    mcl.out.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * @requires message != null
     * @param message
     * sends the message to every client.
     */
    public void broadcastMessageAll(String message){
        for (ClientHandler mcl: controller.clientMap.keySet()) {
            try {
                mcl.out.write("SERVER: " + message);
                mcl.out.newLine();
                mcl.out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * removes client from controller's list
     * @ensures client is removed from ClientMap
     */
    public void removeClientHandler(){
        controller.clientMap.remove(this);
        try {
            this.out.close();
            this.socket.close();
            this.in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        broadcastMessage("SERVER: Player " + this.username + " has left the game");

    }

}
