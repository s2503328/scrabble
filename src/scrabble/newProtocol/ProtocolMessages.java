package scrabble.newProtocol;

public class ProtocolMessages {
    public static final String MOVE= "MOVE";
    public static final String SWAP = "SWAP";
    public static final String JOIN = "JOIN";
    public static final String READY = "READY";
    public static final String DELIMITER = ";";
}
