package scrabble.newProtocol;



import scrabble.controller.Controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    // needs the controller
    private Controller controller;
    private ServerSocket serverSocket;
    private Socket socket;

    /**
     *
     * @param serverSocket
     * constructor of the class
     */
    public Server(ServerSocket serverSocket){
        this.serverSocket = serverSocket;
        this.controller = new Controller();

    }


    @Override
    public void run() {
        try {
            //constantly listens for requests
            while (true) {
                socket = serverSocket.accept();
                System.out.println("A client has connected.");
                System.out.println("players size = " + controller.getClientMap().size());
                System.out.println("clienthandlers size = " + controller.getClientMap().size());
                // creates a clientHandler for the newcomer
                ClientHandler handler = new ClientHandler(socket, controller);


                new Thread(handler).start();


            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Controller getController(){
        return controller;
    }

    /**
     * @requires server !=null
     * @ensures server is closed
     */
    public void shutDown(){
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(1234);
        Server server = new Server(serverSocket);
        new Thread(server).start();

    }
}
