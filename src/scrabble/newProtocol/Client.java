package scrabble.newProtocol;

import scrabble.exceptions.ExitProgram;
import scrabble.exceptions.ServerUnavailableException;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private Socket clientSocket;
    private BufferedWriter out;
    private BufferedReader in;
    private String username;

    /**
     * constructor of the client
     * @requires username != null
     * @param username name of client
     */
    public Client(String username) {
        this.username = username;
        clientSocket = null;
        out = null;
        in = null;
    }

    public BufferedReader getIn() {
        return in;
    }

    public BufferedWriter getOut() {
        return out;
    }

    /**
     *
     *
     * tries to connect to the server.
     */
    public void createConnection()  {
        clearConnection();
        while (clientSocket == null) {
            String host = "localhost";
            int port = 1234;

            // try to open a Socket to the server
            try {
                InetAddress addr = InetAddress.getByName(host);
                System.out.println("Attempting to connect to " + addr + ":"
                        + port + "...");
                clientSocket = new Socket(addr, port);
                in = new BufferedReader(new InputStreamReader(
                        clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(
                        clientSocket.getOutputStream()));
                if (clientSocket.isConnected()) {
                    System.out.println("Connected to " + addr + ":" + port + ".");
                }
            } catch (IOException e) {
                System.out.println("ERROR: could not create a socket on "
                        + host + " and port " + port + ".");


            }
        }
    }

    /**
     * Resets the serverSocket and In- and OutputStreams to null.
     * @ensures clientSocket == null && in == null && out == null
     * Always make sure to close current connections via shutdown()
     * before calling this method!
     */
    public void clearConnection() {
        clientSocket = null;
        in = null;
        out = null;
    }


    /**
     * Sends a message to the connected server, followed by a new line.
     * The stream is then flushed.
     * @requires msg != null
     * @param msg the message to write to the OutputStream.
     * @throws ServerUnavailableException if IO errors occur.
     */
    public synchronized void sendMessage(String msg)
            throws ServerUnavailableException {
        if (out != null) {
            try {
                out.write(msg);
                out.newLine();
                out.flush();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                throw new ServerUnavailableException("Could not write "
                        + "to server.");
            }
        } else {
            throw new ServerUnavailableException("Could not write "
                    + "to server.");
        }
    }

    /**
     * Reads and returns one line from the server.
     *
     * @return the line sent by the server.
     * @throws ServerUnavailableException if IO errors occur.
     */
    public void readLineFromServer()
            throws ServerUnavailableException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String messageFromServer;
                    while (clientSocket.isConnected()) {
                        try {
                        messageFromServer = in.readLine();
                        if (messageFromServer == null){
                            break;
                        }
                        else {
                            System.out.println(messageFromServer);
                            System.out.flush();
                        }
                    }
                        catch (IOException e) {
                            closeConnection();
                        }
                }
                }
            }).start();
    }



    /**
     * closes the connection of client when an IO exception happens in client
     */
    public void closeConnection() {
        System.out.println("Closing the connection...");
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your username to join the game:");
        String msg = scanner.nextLine();
        Client client = new Client(msg);

        try {
            client.createConnection();

            client.readLineFromServer();
            while (msg != null) {
                try {
                    client.sendMessage(msg);

                    if (scanner.hasNextLine()){
                        msg = scanner.nextLine();
                    }
                } catch (ServerUnavailableException | NullPointerException e) {
                    e.printStackTrace();
                }


            }
        } catch (ServerUnavailableException e) {
            e.printStackTrace();


        }
    }
}

