package scrabble.controller;

import scrabble.model.Game;
import scrabble.model.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ScrabbleLocal {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean playGame = true;
        while (playGame){
            List<Player> players = new ArrayList<>();
            if (args.length >= 2) {
                for (int i = 0; i < args.length; i++) {
                    Player player1 = new Player(args[0]);
                    Player player2 = new Player(args[1]);
                    players.add(player1);
                    players.add(player2);
                    if (args.length == 3) {
                        Player player3 = new Player(args[2]);
                        players.add(player3);
                    }
                    if (args.length == 4) {
                        Player player3 = new Player(args[2]);
                        Player player4 = new Player(args[3]);
                        players.add(player3);
                        players.add(player4);
                    }
                }
            }
            else {
                System.out.println("Not enough players to play the game.");
                break;
            }
            Game game = new Game(players);
            game.play();
            System.out.println("Game over! Winner is: " + game.isWinner().getName() + " with " + game.isWinner().score + " points!");
            System.out.println("\n Game ended. Play again? (y/n)");
            String answer = scanner.next();
            if (answer.toLowerCase().equals("y")){
                playGame = true;
            }
            else {
                playGame = false;
                break;
            }
            game.reset();

        }


    }

}
