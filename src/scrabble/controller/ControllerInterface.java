package scrabble.controller;

import scrabble.exceptions.InvalidDirectionException;
import scrabble.model.Tiles;
import scrabble.newProtocol.ClientHandler;

import java.util.List;

public interface ControllerInterface {

    /**
     *
     * @param word
     * @param pos
     * @param direction
     * does the move command given by client.
     */
    public void doMove(String word, String pos, String direction, ClientHandler ch) throws InvalidDirectionException;

    /**
     *
     * @param tiles
     * swaps the tiles user requested.
     */
    void doSwap(List<Tiles> tiles,ClientHandler ch);

    /**
     *
     * @return the new state of the board after the move.
     */
    void updateBoard(ClientHandler clientHandler);

    /**
     * starts the game when there is enough players.
     */
    void startGame();

    /**
     * resets the game.
     */
    void Reset();

    void Ready();
}
