package scrabble.controller;

import scrabble.controller.ControllerInterface;
import scrabble.exceptions.InvalidDirectionException;
import scrabble.exceptions.SquareFullException;
import scrabble.model.*;
import scrabble.newProtocol.ClientHandler;
import scrabble.view.PrinterBoard;

import java.io.IOException;
import java.util.*;

public class Controller implements ControllerInterface {

    public Map<ClientHandler, Player> clientMap = new HashMap<ClientHandler, Player>();

    // needs the view to send to client.
    private PrinterBoard view;

    public Game game;
    private int playerReadyCount = 0;
    private List<Player> players = new ArrayList<>();

    /**
     * set the players needed for the game
     * @ensures players =! null
     */
    public void setPlayers() {
        for (ClientHandler ch : clientMap.keySet()){
            players.add(clientMap.get(ch));
        }
    }

    public Map<ClientHandler, Player> getClientMap() {
        return clientMap;
    }

    /**
     * @requires word != null && pos != null && direction != null && ch != null
     * @param word word played by client
     * @param pos position chosen by client
     * @param direction direction chosen by client
     * @param ch current player/client
     * plays the move according to the current player(client)'s input.
     */
    @Override
    public synchronized void doMove(String word, String pos, String direction,ClientHandler ch) {

        if ( !game.gameOver()) {
            if (ch.player == game.getCurrentPlayer()) {

                //if direction is not entered as per instructions, inform player
                if (!direction.toUpperCase().equals("HOR") && !direction.toUpperCase().equals("VER")) {
                    try {
                        ch.out.write(view.invalidDir());
                        return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                //check if word is valid and tiles are either in the player's rack or already on the board
                if (game.validityWord(word) && game.tilesCheck(word, pos, direction)) {

                        ArrayList<PositionWord> newWords = game.findNewWords(word, pos, direction);
                        boolean newWordsCorrect = true;
                        for (PositionWord w : newWords) {
                            if (!game.validityWord(w.getWord())) {
                                newWordsCorrect = false;
                                break;
                            }
                        }
                        if (newWordsCorrect) {
                            for (PositionWord w : newWords) {
                                if (direction.toUpperCase().equals("HOR")) {
                                    //calculate score for VER
                                    game.calculateScore(w.getWord(), w.getStartRow(), w.getStartCol(), "VER");

                                } else {
                                    //calculate score for HOR
                                    game.calculateScore(w.getWord(), w.getStartRow(), w.getStartCol(), "HOR");
                                }
                            }
                            //calculate score for inputted word, and then place the word on the board
                            game.calculateScore(word, pos, direction);
                            try {
                                game.placeTiles(word, pos, direction);
                            } catch (SquareFullException e) {
                                e.printStackTrace();
                            }

                        }
                        game.giveTiles(ch.player);
                    } else {
                    try {
                        //if the move player tried to make was invalid, inform player
                        ch.out.write(view.invalidMove());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                game.NextPlayer();
                showBoard();
            }
            else{
                try {
                    //if client is not current player, and they want to make a move, inform them that they need to wait their turn
                    ch.out.write(view.waitingTurn());
                    ch.out.newLine();
                    ch.out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            //client cannot make a move if the game is over
            try {
                ch.out.write("Game is over, you cannot make moves.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * show the board and inform players of their tiles, as well as if it is their turn or not
     * @requires board != null && clientMap != null && players != null
     * @throws IOException
     */
    public void showBoard(){
        try{
        for (ClientHandler handler : clientMap.keySet()) {
            //if the client is the current player, inform them of their tiles and score, as well as ask them to enter a command
            if (handler.player == game.getCurrentPlayer()) {
                updateBoard(handler);

                handler.out.write(view.showPlayer(game.getCurrentPlayer()));
                handler.out.newLine();
                handler.out.flush();

                handler.out.write(view.askForMove());
                handler.out.newLine();
                handler.out.flush();

            }
            //if the client is not the current player, then inform them of that, and show them their tiles and score
            else {
                updateBoard(handler);

                handler.out.write(view.waitingTurn());
                handler.out.write(view.showPlayer(handler.player));
                handler.out.newLine();
                handler.out.flush();

            }
        }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }



    /**
     * @requires ch != null && tiles != null
     * @param tiles
     * @param ch
     * swaps the tiles according to the current player(client)'s input.
     */
    @Override
    public synchronized void doSwap(List<Tiles> tiles,ClientHandler ch) {
        game.swap(tiles);
        game.NextPlayer();
        showBoard();
    }

    /**
     * @requires  client != null
     * @param client
     * @throws IOException
     *  sends the help menu to client.
     */
    public synchronized void doHelp(ClientHandler client) throws IOException {
        client.out.write(view.helpMenu());
    }

    /**
     * @requires clientHandler != null
     * @param clientHandler
     * sends the updated board to player.
     */
    @Override
    public synchronized void updateBoard(ClientHandler clientHandler) {

        try {
            clientHandler.out.newLine();
            clientHandler.out.write(view.makeBoard());
            clientHandler.out.newLine();
            clientHandler.out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * creates a new game in controller,
     * initiates the game.
     * @ensures game != null
     */
    @Override
    public synchronized void startGame() {
        setPlayers();
        game = new Game(players);
        view = new PrinterBoard();
        view.setGame(game);

        for (ClientHandler handler: clientMap.keySet()){
            game.giveTiles(handler.player);

            //if the client is the current player, inform them of their tiles and score, as well as ask them to enter a command
            if (handler.player == game.getCurrentPlayer()){
                updateBoard(handler);
                try {
                    handler.out.write(view.showPlayer(game.getCurrentPlayer()));
                    handler.out.newLine();
                    handler.out.flush();

                    handler.out.write(view.askForMove());
                    handler.out.newLine();
                    handler.out.flush();

                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            }
            else{
                updateBoard(handler);

                //if the client is not the current player, then inform them of that, and show them their tiles and score
                try {
                    handler.out.write(view.waitingTurn());
                    handler.out.write(view.showPlayer(handler.player));
                    handler.out.newLine();
                    handler.out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

       }

    /**
     * calls the reset method from the game class
     * resets the game, makes the board empty
     * @requires players != null
     */
    @Override
    public synchronized void Reset() {
        game.reset();
    }

    /**
     * @requires clientMap != null
     * increments the players ready for the game. if the count of ready players equals to players in the server
     * it starts the game.
     */
    @Override
    public synchronized void Ready(){
        playerReadyCount++;
        if (playerReadyCount == clientMap.size()){
                startGame();
        }

    }

    /**
     * skips the turn of current player.
     * @requires input from user == skip
     * @ensures skip == true
     */
    public synchronized void Skip(){
        game.getCurrentPlayer().setSkip(true);
        game.NextPlayer();
        showBoard();
    }

    /**
     * @requires game.gameOver = true
     * informs players that the game is over and who the winner is, with the winning score.
     */
    public synchronized void declareWinner() throws IOException {
        Player winner = game.isWinner();

        for (ClientHandler client: clientMap.keySet()){
            client.out.write(view.gameOverMessage(winner));
            client.out.newLine();
            client.out.flush();
        }
    }

}
