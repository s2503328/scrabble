package scrabble.model;

import java.util.*;

import static scrabble.model.Tiles.*;

public class TilesBag {

    private Map<Tiles, Integer> tiles = tileCount();
    private List<Tiles> tilesList = tileAmount();


    //tiles and their amount each game
    public Map<Tiles, Integer> tileCount(){
        Map<Tiles, Integer> tileCount = new HashMap<>();
        tileCount.put(A, 9);
        tileCount.put(B, 2);
        tileCount.put(C, 2);
        tileCount.put(D, 4);
        tileCount.put(E, 12);
        tileCount.put(F, 2);
        tileCount.put(G, 2);
        tileCount.put(H, 2);
        tileCount.put(I, 8);
        tileCount.put(J, 2);
        tileCount.put(K, 2);
        tileCount.put(L, 4);
        tileCount.put(M, 2);
        tileCount.put(N, 6);
        tileCount.put(O, 8);
        tileCount.put(P, 2);
        tileCount.put(Q, 1);
        tileCount.put(R, 6);
        tileCount.put(S, 4);
        tileCount.put(T, 6);
        tileCount.put(U, 4);
        tileCount.put(V, 2);
        tileCount.put(W, 2);
        tileCount.put(X, 1);
        tileCount.put(Y, 2);
        tileCount.put(Z, 1);
        tileCount.put(BLANK, 2);

        return tileCount;
    }



    //tiles and their points per tile
    public Map<Tiles, Integer> tileValue(){
        Map<Tiles, Integer> tileValue = new HashMap<>();
        tileValue.put(A, 1);
        tileValue.put(B, 3);
        tileValue.put(C, 3);
        tileValue.put(D, 2);
        tileValue.put(E, 1);
        tileValue.put(F, 4);
        tileValue.put(G, 1);
        tileValue.put(H, 4);
        tileValue.put(I, 1);
        tileValue.put(J, 8);
        tileValue.put(K, 5);
        tileValue.put(L, 1);
        tileValue.put(M, 3);
        tileValue.put(N, 1);
        tileValue.put(O, 1);
        tileValue.put(P, 3);
        tileValue.put(Q, 10);
        tileValue.put(R, 1);
        tileValue.put(S, 1);
        tileValue.put(T, 1);
        tileValue.put(U, 1);
        tileValue.put(V, 4);
        tileValue.put(W, 4);
        tileValue.put(X, 8);
        tileValue.put(Y, 4);
        tileValue.put(Z, 10);
        tileValue.put(BLANK, 0);

        return tileValue;
    }


    //the map of tiles and their amounts made into a list
    public List<Tiles> tileAmount(){
        List<Tiles> tilesList = new ArrayList<>();
        int count;
        Random r = new Random();
        for (Tiles tile: tiles.keySet()){
            count = tiles.get(tile);
            for(int i = 0; i < count; i++){
                int randomIndex = 0;
                if (tilesList.size() > 0){
                    randomIndex = r.nextInt(tilesList.size());
                }
                tilesList.add(randomIndex, tile);
            }
        }
        return tilesList;
    }


    public List<Tiles> getTilesList(){
        return tilesList;
    }

    /**
     * if the tile has been picked, its amount left/available will be decreased by 1.
     * if it has reached 0, it will no longer be available to be sent to the player.
     * get rid o used up letters
     * reduce the value of letter each time it is used
     * @ensures generated tile is removed from tiles bag
     * @return generated tile
     */
    public Tiles getRandomTile() {
        if (tilesList.size() > 0){
            return tilesList.remove(0);
        }
        return null;
    }

}
