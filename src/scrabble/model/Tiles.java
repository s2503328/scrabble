package scrabble.model;

import java.util.*;

/**
     * there are 100 tiles in total
     * Amount of each tile varies:
     * 1x(Q, X, Z)
     * 2x(B, C, F, G, H, J, K, M, P, V, W, Y, BLANK)
     * 4x(D, L, S, U)
     * 6x(N, R, T)
     * 8x(I, O)
     * 9x(A)
     * 12x(E)
     */

public enum Tiles {

        A, B, C, D, E, F, G, H, I, J, K , L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, BLANK;




}
