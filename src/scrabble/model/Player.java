package scrabble.model;

import java.util.ArrayList;


public class Player {
    private String name;
    public int score;
    public ArrayList<Tiles> rack;
    private Board board;
    public boolean skip = false;

    /**
     * player will have 7 tiles in total in their rack at each new turn
     * @requires name != null
     * @ensures player != null
     */
    public Player(String name){
        this.name = name;
        this.score = 0;
        rack = new ArrayList<Tiles>(7);
    }

    /**
     * @requires board != null
     * @param board board of current game
     */
    public void setBoard(Board board) {this.board = board;}

    /**
     * @requires skip != null
     * @param skip
     */
    public void setSkip(boolean skip){
        this.skip = skip;
    }

    /**
     * @ensures name != null
     * @return name of player
     */
    public String getName() {
        return name;
    }

}
