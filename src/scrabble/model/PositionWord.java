package scrabble.model;

public class PositionWord {
    private String word;
    private int startRow;
    private int startCol;

    /**
     * creates an object which is used to store the new words created/found connected to played word when making a move, as well their start row and start column
     * @requires word != null && startRow != null && startCol != null
     * @ensures PositionWord != null
     * @param word inputted word
     * @param startRow starting row of the word
     * @param startCol starting column of the word
     */
    public PositionWord(String word, int startRow, int startCol){
        this.word = word;
        this.startRow = startRow;
        this.startCol = startCol;
    }

    public String getWord(){ return word;}

    public int getStartRow() {
        return startRow;
    }

    public int getStartCol() {
        return startCol;
    }
}
