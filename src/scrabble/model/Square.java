package scrabble.model;

import scrabble.view.ANSI;

public class Square {

    private int multiplier;
    private String multiplierType;
    private Tiles tileType;
    private String color;
    private boolean available;
    private Tiles blankLetter;


    /**
     * creates a square object
     * @requires multiplier != null
     * @param multiplier the multiplier of the square, can be either 1, 2, or 3
     * @param tileType what tile the square has, if empty square, tileType is null
     * @param multiplierType if the square has a multiplier type, aka if it is a premium tile that can multiply a word or a letter. if no multiplier type, set to null
     * @param color the color orf the square
     */
    public Square(int multiplier, Tiles tileType, String multiplierType, String color){
        //normal multiplier is 1.
        this.multiplier = multiplier;
        this.multiplierType = multiplierType;
        this.tileType = tileType;
        this.color = color;
        this.available = true;
    }

    /**
     * multiplies the word placed over this square by 3
     * @ensures multiplier == 3 && multiplierType == WORD && color != null
     */
    public void DarkRedSquare(){
        this.multiplier = 3;
        this.multiplierType = "WORD";
        this.color = ANSI.RED_BACKGROUND;
    }

    /**
     * multiplies the word placed over this square by 2
     * @ensures multiplier == 2 && multiplierType == WORD && color != null
     */
    public void PinkSquare(){
        this.multiplier = 2;
        this.multiplierType = "WORD";
        this.color = ANSI.PURPLE_BACKGROUND_BRIGHT;
    }

    /**
     * multiplies the letter placed over this square by 3
     * @ensures multiplier == 3 && multiplierType == Letter && color != null
     */
    public void DarkBlueSquare(){
        this.multiplier = 3;
        this.multiplierType = "LETTER";
        this.color = ANSI.BLUE_BACKGROUND;
    }

    /**
     * multiplies the letter placed over this square by 2
     * @ensures multiplier == 2 && multiplierType == Letter && color != null
     */
    public void BlueSquare(){
        this.multiplier = 2;
        this.multiplierType = "LETTER";
        this.color = ANSI.CYAN_BACKGROUND;
    }

    /**
     * gets the multiplier of the square
     * @return multiplier
     */
    public int getMultiplier() {
        return multiplier;
    }

    /**
     * gets the multiplierType of the square
     * @return multiplierType
     */
    public String getMultiplierType() { return multiplierType;}

    /**
     * gets the letter placed on the square
     * @return tileType
     */
    public Tiles getTileType() {
        return tileType;
    }

    /**
     * sets the tile to the chosen tile
     * @param tileType letter chosen
     */
    public void setTileType(Tiles tileType) {
        this.tileType = tileType;
    }

    /**
     * @invariant available != null
     * checks if the square is empty or if it has a tile on it
     * @return state of square
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * once a tile has been placed on a square, change its state to not available
     * @invariant available != null
     */
    public void setAvailable() {
        available = !available;
    }

    /**
     * if a blank tile has been played, return the letter which replaced it
     * @return replaced letter
     */
    public Tiles getBlankLetter() {
        return blankLetter;
    }

    /**
     * if a blank tile was played, place the chosen letter to replace the blank tile on that square
     * @invariant blankLetter != null
     * @param blankLetter letter replacing blank tile
     */
    public void setBlankLetter(Tiles blankLetter) {
        this.blankLetter = blankLetter;
    }
}
