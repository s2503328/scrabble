package scrabble.model;


import scrabble.exceptions.SquareFullException;
import scrabble.givenWordCheck.java.FileStreamScrabbleWordChecker;
import scrabble.givenWordCheck.java.InMemoryScrabbleWordChecker;
import scrabble.givenWordCheck.java.ScrabbleWordChecker;
import scrabble.newProtocol.ClientHandler;
import scrabble.view.PrinterBoard;

import java.util.*;

import static scrabble.model.Board.DIM;

public class Game {

    private List<Player> players;
    private TilesBag tilesBag;

    protected int currentPlayer;

    /**
     * The board.
     * @invariant board is never null
     */
    private Board board;

    private PrinterBoard view;

    /**
     * checks if words are valid
     */
    private ScrabbleWordChecker checkWords = new InMemoryScrabbleWordChecker();
    private ScrabbleWordChecker fileStreamChecker = new FileStreamScrabbleWordChecker();


    /**
     * constructor of the class.
     * Creates a new Game Object.
     * creates the desired amount of players (2-4 players)
     * To create a new game, you need a board, players,
     * @requires List<Player> players != null
     * @ensures game object != null
     */
    public Game(List<Player> players){
        //creates a board
        board = new Board();
        tilesBag = new TilesBag();
        view = new PrinterBoard();
        view.setBoard(board);

        //assigns players
        this.players = players;
        for (Player player:players){
            player.score = 0;
            player.setBoard(board);
        }
        Random pick = new Random();
        currentPlayer = pick.nextInt(players.size());

        // give tiles to the starting player
        giveTiles(getCurrentPlayer());

    }

    /**
     * @return tilesBag
     */
    public TilesBag getTilesBag() {
        return tilesBag;
    }

    /**
     * @requires board != null
     * @return the board
     */
    public Board getBoard() {
        return board;
    }

    /**
     * @ensures player != null
     * @return current player
     */
    public Player getCurrentPlayer() {
        return players.get(currentPlayer);
    }

    /**
     * changes the current player of the game
     * @requires currentPlayer != null
     * @ensures currentPlayer changes
     */
    public void NextPlayer(){
        currentPlayer = (currentPlayer + 1) % players.size();
    }

    /**
     * calculates the score of each tile placed during a turn as well as the other tiles it has been connected to
     * include the multipliers in Square
     * @ensures returned score != null
     * @param wordPlayed the word that the player entered
     * @return score for word
     */
    public int calculateScore(String wordPlayed,String position, String direction){
        int[] pos = board.stringToPosition(position);
        int row = pos[0];
        int col = pos[1];
        return calculateScore(wordPlayed, row,col, direction);
    }


    /**
     * calculates the score of each tile placed during a turn as well as the other tiles it has been connected to
     * include the multipliers in Square
     * @ensures returned score != null
     * @param wordPlayed the word that the player entered
     * @return score for word
     */
    public int calculateScore(String wordPlayed,int row, int col, String direction){


        int total = 0;
        int multiplier = 1;
        int placedTiles = 0;

        String[] splitWord = wordPlayed.split("");

        boolean prevWasZero = false;
        for (String s : splitWord) {
            s = s.toUpperCase();

            //if the previous letter/character was a 0, skip this letter as it should not be counted in the score as it is a blank tile
            if (prevWasZero){
                prevWasZero = false;
                continue;
            }
            if (board.getSquare(row, col).isAvailable()){
                if ("WORD".equals(board.getSquare(row, col).getMultiplierType())) {
                    multiplier *= board.getSquare(row, col).getMultiplier();
                }
                if (s.equals("0")){
                    //no points, multiplier should still be counted for the word
                    prevWasZero = true;
                }
                else {
                    if ("LETTER".equals(board.getSquare(row, col).getMultiplierType())){
                        total += (tilesBag.tileValue().get(Tiles.valueOf(s))) * board.getSquare(row, col).getMultiplier();
                    }
                    else {
                        total += (tilesBag.tileValue().get(Tiles.valueOf(s)));
//                        System.out.println("total is: " + total + " for " + row + ", " + col);
                    }
                }
                placedTiles++;
            }
            else{
                total += (tilesBag.tileValue().get(Tiles.valueOf(s)));
//                System.out.println("existing tile has total: " + total + " for " + row + ", " + col);
            }

            if (direction.toUpperCase().equals("HOR")){
                col++;
            }
            else if (direction.toUpperCase().equals("VER")) {
                row++;
            }

        }


        total *= multiplier;
        if (placedTiles == 7){
            total += 50;
        }
        players.get(currentPlayer).score += total;
        return total;
    }



    /**
     * for each letter, check if there are tiles before or after in the opposite direction of the placed word.
     * if more letters in opposite direction exists, check each for validityWord and calculate score of each new word
     * @requires word != null
     * @param word word to be played
     * @param position position of word to be played
     * @param direction direction of word to be played
     * @return arraylist of words that are possibly created with the entered word to be played
     */
    public ArrayList<PositionWord> findNewWords(String word, String position, String direction) {
        int[] pos = board.stringToPosition(position);
        int row = pos[0];
        int col = pos[1];

        ArrayList<PositionWord> positionWords = new ArrayList<>();


        String[] wordSplit = word.split("");
        for (String s: wordSplit){
            StringBuilder newWord;
            if (direction.toUpperCase().equals("HOR")){
                //check each letter placed during current turn (which is HOR) in direction VER
                if(board.getSquare(row, col).isAvailable()){
                    newWord = new StringBuilder(s);


                    // checks the LOWER side of the tile
                    int tempRowDown = row + 1;
                    while(!board.getSquare(tempRowDown,col).isAvailable() && tempRowDown < DIM ){
                        newWord.append(board.getSquare(tempRowDown, col).getTileType().toString());
                        tempRowDown++;
                    }


                    // checks the UPPER side of the tile
                    int tempRowUp = row - 1;
                    while (!board.getSquare(tempRowUp,col).isAvailable() && tempRowUp >= 0){
                            newWord.insert(0, board.getSquare(tempRowUp, col).getTileType().toString());
                            if (tempRowUp == 0){
                                break;
                            }
                            tempRowUp--;
                    }
                    if (tempRowDown < row -1 || tempRowUp > row +1){
                        PositionWord foundWord = new PositionWord(newWord.toString(), tempRowUp-1, col);
                        positionWords.add(foundWord);
                    }
                }
                col++;
            }
            else if (direction.toUpperCase().equals("VER")){
                //check each letter placed during current turn (which is VER) in direction HOR
                if(board.getSquare(row, col).isAvailable()){
                    newWord = new StringBuilder("s");

                    //checks the LEFT side of the tile
                    int tempColLeft = col - 1;
                        while (!board.getSquare(row, tempColLeft).isAvailable() && tempColLeft >= 0){
                            newWord.insert(0, board.getSquare(row, tempColLeft).getTileType().toString());
                            if (tempColLeft== 0) {
                                break;
                            }
                            tempColLeft--;
                        }


                    //checks the RIGHT side of the tile
                    int tempColRight = col + 1;
                        while (!board.getSquare(row, tempColRight).isAvailable() && tempColRight < DIM){
                            newWord.append(board.getSquare(row, tempColRight).getTileType().toString());
                            tempColRight++;
                        }
                    if (tempColLeft< col -1 || tempColRight > col +1){
                        PositionWord foundWord = new PositionWord(newWord.toString(), row, tempColLeft-1);
                        positionWords.add(foundWord);
                    }

                }
                row++;
            }

        }

        return positionWords;
    }


    /**
     * checks if the word player wants to make is valid
     * @requires word != null
     * @param word word to be played
     * @return true if word exists
     */
    public boolean validityWord(String word){

        String checkWord = word.replaceAll("0","");
        if (checkWords.isValidWord(checkWord) != null && fileStreamChecker.isValidWord(checkWord) != null) {
            return true;
        }
        return false;

    }

    /**
     * checks if the letters in the word the player wants to make is either in their rack or on the board
     * @requires word != null
     * @param word the player wants to play
     * @param position starting square the player places the first letter of the word
     * @param direction if player is placing the word horizontally or vertically
     * @return true if letters to make word are in rack or on the board
     */
    public boolean tilesCheck(String word, String position, String direction) {

        int[] pos = board.stringToPosition(position);
        int row = pos[0];
        int col = pos[1];

        boolean foundPrevPlacedTile = false;

        String[] splitWord = word.split("");
        boolean prevWasZero = false;
        for (String s : splitWord) {
            if (prevWasZero){
                prevWasZero = false;
                continue;
            }
            s = s.toUpperCase();
            //if there is a tile on the board at that specific square, check if the tile on that square is the same as current letter in the inputted word, if it is, set foundPrevPlacedTile to true, as there is a tile on this square
            if (!board.getSquare(row, col).isAvailable()){
                String letterTile = board.getSquare(row, col).getTileType().toString().toUpperCase();
                if (!s.equals(letterTile)){
                    if (!s.equals("0")){
//                        System.out.println("Sorry, it seems that tile('"+s+"') isn't on the board where you placed it, thus you cannot play '" + word + "'");
                    }
                    return false;
                }
                foundPrevPlacedTile = true;
            }
            //if the current character/letter has a 0 in it, check if player has a blank tile in their rack, if they do, set prevWasZero to true, as the letter is a 0
            else if (s.equals("0")){
                if (!players.get(currentPlayer).rack.contains(Tiles.BLANK)){
//                    System.out.println("Sorry, you don't seem to have a blank tile in your rack.");
                    return false;
                }
                prevWasZero = true;
            }
            //if the player does not have the letter they want to play in their rack, and it is not on the board, return false, as the move cannot be done
            else if (!players.get(currentPlayer).rack.contains(Tiles.valueOf(s))){
//                System.out.println("Sorry, you don't seem to have that tile ('" + s +"') in your rack.");
                return false;
            }
            if (direction.toUpperCase().equals("HOR")){
                col++;
            }
            else if (direction.toUpperCase().equals("VER")) {
                row++;
            }
        }
        //check if the word is connected to words already on the board.
        if(!foundPrevPlacedTile){
            for (int x = 0; x < DIM; x++){
                for (int y = 0; y <DIM; y++){
                    if (!board.getSquare(y, x).isAvailable()){
//                        System.out.println("Not connected to previously placed word.");
                        return false;
                    }
                }
            }
        }
        return true;
    }


    /**
     * places tiles on the board and removed them from the player's rack.
     * @requires word to be valid
     * @requires word != null
     * @param word word the player wants to play
     * @param position starting square the player places the first letter of the word
     * @param direction if player is placing the word horizontally or vertically
     */
    public void placeTiles(String word, String position, String direction) throws SquareFullException {
        int[] pos = board.stringToPosition(position);
        int row = pos[0];
        int col = pos[1];

        String[] splitWord = word.split("");

        boolean prevWasZero = false;
        for (String s : splitWord) {
            s = s.toUpperCase();

            //check if the previous written character, aka s, had a 0 in it, and if it did, set the blank letter to the inputted letter after the 0
            if (prevWasZero){
                board.placeTile(row, col, Tiles.BLANK);
                board.getSquare(row, col).setBlankLetter(Tiles.valueOf(s));
                prevWasZero = false;
            }
            else if (board.getSquare(row, col).isAvailable()){
                //if the current character is 0, set prevWasZero value to true, remove the blank tile from player's rack (as 0letter is how blank tiles are inputted), and jump to the next s.
                if (s.equals("0")){
                    players.get(currentPlayer).rack.remove(Tiles.BLANK);
                    prevWasZero = true;
                    continue;
                }
                //if it is not a 0, and not the letter after a 0, just place that tile on the board and remove it from the player's rack.
                else {
                    board.placeTile(row, col, Tiles.valueOf(s));
                    players.get(currentPlayer).rack.remove(Tiles.valueOf(s));
                }
            }
            if (direction.toUpperCase().equals("HOR")){
                col++;
            }
            else if (direction.toUpperCase().equals("VER")) {
                row++;
            }

        }

    }




    /**
     * sends a random tile to the player who ended its turn.
     * Player needs to have 7 tiles at all times, thus method needs to send amount of tiles that gets the player 7 tiles in total.
     * @requires players != null
     */
    public void giveTiles(Player player) {

        while(player.rack.size()<7){
            try{
                player.rack.add(tilesBag.getRandomTile());
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }


    }

    /**
     * @requires tiles != null
     * @ensures chosen tiles are swapped for new tiles
     * @param tiles tiles wanted to be swapped
     */
    public void swap(List<Tiles> tiles){
        if (tilesBag.getTilesList().size() < tiles.size()){
            throw new IllegalArgumentException("tilebag does not have the amount of tiles to swap your requested tiles.");
        }
        for (Tiles t : tiles){
            players.get(currentPlayer).rack.remove(t);
            t = tilesBag.getRandomTile();
            players.get(currentPlayer).rack.add(t);
        }
    }

    /**
     * resets the game, makes the board empty
     * @requires players != null
     */
    public void reset(){
        Game game = new Game(players);
    }

    /**
     * Finds winner after game has ended, adds points to score if player has no tiles left in rack,
     * and subtracts points if player has tiles left, then checks for winner again. If it is a tie, return winner before
     * changing scores and that winner's score before changing, if there is a winner, return that winner. If no winner, return null.
     * @requires players != null
     */
    public Player isWinner(){

        List<Player> playersRanked = new ArrayList<>();
        List<Integer> scoreRanked = new ArrayList<>();

        Player winner = null;


        //add players to a new list, with the first player being the player with the highest score
        if (players.size() > 1){
            for (Player p: players){
                if (p == players.get(0)){
                    playersRanked.add(0, p);
                    scoreRanked.add(0, p.score);
                    continue;
                }
                boolean inserted = false;
                for (int a = 0; a < playersRanked.size(); a++){
                    if (p.score > playersRanked.get(a).score){
                        playersRanked.add(a, p);
                        scoreRanked.add(a, p.score);
                        inserted = true;
                        break;
                    }
                }
                if (!inserted){
                    scoreRanked.add(p.score);
                    playersRanked.add(p);
                }
            }
        }


        //add and/or subtract points based on if player has tiles left in their rack or not
        int total =0;
        for (Player p: players){
            if (!(p.rack == null || p.rack.isEmpty())){
                for (Tiles t: p.rack){
                    p.score -= (tilesBag.tileValue().get(Tiles.valueOf(t.toString())));
                    total += (tilesBag.tileValue().get(Tiles.valueOf(t.toString())));
                }
            }
            if (p.rack == null || p.rack.isEmpty()){
                p.score += total;
            }
        }

        List<Player> changedPlayerRank = new ArrayList<>();

        //add players to a new list, with the first player being the player with the highest score
        if (players.size() > 1){
            for (Player p: players){
                if (p == players.get(0)){
                    changedPlayerRank.add(0, p);
                    continue;
                }
                boolean inserted = false;
                for (int a = 0; a < changedPlayerRank.size(); a++){
                    if (p.score > changedPlayerRank.get(a).score){
                        changedPlayerRank.add(a, p);
                        inserted = true;
                        break;
                    }
                }
                if (!inserted){
                    changedPlayerRank.add(p);
                }
            }
        }

        //if changedPlayerRank has no winner, meaning it is a tie, return player with  the highest score of playersRanked.
        //if no winner in playersRanked, return null.
        if (changedPlayerRank.get(0).score == changedPlayerRank.get(1).score ){
            if (playersRanked.get(0).score == playersRanked.get(1).score ){
                winner = null;
            }
            else {
                playersRanked.get(0).score = scoreRanked.get(0);
                winner = playersRanked.get(0);
            }
        }
        else {
            winner = changedPlayerRank.get(0);
        }

        return winner;
    }



    /**
     * game is played until the game is over
     * @requires gameOver != false when starting
     */
    public void play() {
        update();
        while (!gameOver()){
            giveTiles(getCurrentPlayer());
            System.out.println(view.showPlayer(getCurrentPlayer()));
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please enter a word, starting tile position, and direction (HOR or VER) (separate with a ';'): ");

            String input = scanner.nextLine();
            String[] command = input.split(";");
            String word = command[0];
            String position = command[1];
            String direction = command[2];
            //if player doesn't input a word, it counts as a skip (on local game)
            if (word == null || word.length() == 0){
                players.get(currentPlayer).setSkip(true);
                System.out.println("Turn is skipped. Next player's turn.");
                currentPlayer = (currentPlayer + 1) % players.size();
                continue;
            }
            else {
                players.get(currentPlayer).setSkip(false);
            }
            //check if word is valid and tiles are either in the player's rack or already on the board
            if (validityWord(word) && tilesCheck(word, position, direction)) {
                ArrayList<PositionWord> newWords = findNewWords(word, position, direction);
                boolean newWordsCorrect = true;
                for (PositionWord w: newWords){
                    if(!validityWord(w.getWord())){
                        newWordsCorrect = false;
                        break;
                    }
                }
                if(newWordsCorrect){
                    for (PositionWord w: newWords) {
                        if (direction.toUpperCase().equals("HOR")){
                            //calculate score for VER
                            calculateScore(w.getWord(), w.getStartRow(), w.getStartCol(), "VER");

                        }
                        else {
                            //calculate score for HOR
                            calculateScore(w.getWord(), w.getStartRow(), w.getStartCol(), "HOR");
                        }
                    }
                    calculateScore(word, position, direction);
                    try {
                        placeTiles(word, position, direction);
                    } catch (SquareFullException e) {
                        e.printStackTrace();
                    }
                }
                giveTiles(getCurrentPlayer());
            }
            else{
                System.out.println(view.invalidMove());
            }
            update();
            if (!tilesCheck(word, position, direction)){
                if (!word.contains("0")){
                    System.out.println(view.invalidMove());
                }
            }
            System.out.println(view.scoreAfterMove(getCurrentPlayer()));
            NextPlayer();

        }

    }

    /**
     * if there are no more tiles to be handed out, no more tiles for players to play,
     * and no more valid words to be played with tiles that are left in player's racks, it is game over.
     * @requires players != null
     * @ensures true if all players skipped their turn
     * @ensures true if no tiles left in the tilesBag, nor in the player's rack.
     * @return true if game is over, false if not
     */
    public boolean gameOver(){
        boolean gameOver = false;
        boolean skipValue = true;
        for (Player p: players){
            if (!p.skip){
                skipValue = false;
            }
        }
        //if all players have skipped, game is over
        if (skipValue){
            gameOver = true;
            return gameOver;
        }
        //if the tilesBag is not yet empty, game is not over
        if (!(tilesBag.getTilesList() == null || tilesBag.getTilesList().size() == 0)){
            gameOver = false;
            return gameOver;
        }
        //if the players racks are not yet empty, the game is not over
        if (!(players.get(currentPlayer).rack == null || players.get(currentPlayer).rack.size() == 0)) {
            gameOver = false;
            return gameOver;
        }
        //if tilesBag is empty, and players' racks are empty, then end the game
        gameOver = true;
        return gameOver;
    }

    /**
     * updates the current game situation and prints it out
     * @requires board != null
     */
    public void update(){

        System.out.println(view.makeBoard());
    }


}
