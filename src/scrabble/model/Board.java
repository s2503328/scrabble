package scrabble.model;


import scrabble.view.ANSI;

import java.util.Arrays;

public class Board {

    /**
     * Board is a 15*15, thus the dimensions are DIM*DIM
     */
    public static final int DIM = 15;


    /**
     * 2D array with [row][column]
     * @invariant squares are always DIM*DIM
     */
    private Square[][] squares;

    /**
     * Constructor of the Board.
     * Creates an empty board with specific squares having specific multipliers.
     * Board should have 1-15 numbered as its rows (Horizontals), while it has A-O as its columns (Vertical)
     * @ensures all squares are empty.
     */
    public Board(){
        squares = new Square[DIM][DIM];
        reset();


        //multiplier for word*3 in each corner as well as on A8, O8, H1, and H15
        for (int row = 0; row < DIM ; row += 7 ){
            for (int col = 0; col < DIM ; col += 7){
                if ( row == 7 && col == 7){
                    continue;
                }
                squares[col][row].DarkRedSquare();
            }
        }

        //multiplier for word *2, needs to be on:
        //B2, B14, C3, C13, D4, D12, E5, E11, H8, K5, K11, L4, L12, M3, M13, N2, N14
        for (int row = 1; row < DIM-1 ; row ++ ){
            for (int col = 13; col > 0 ; col--){
                if ((col == 1 && row == 1) || (col == 2 && row == 2) || (col == 3 && row == 3) || (col == 4 && row == 4) || (row == 7 && col == 7)){
                    squares[col][row].PinkSquare();
                }
                if ((col == 1 && row == 13) || (col == 2 && row == 12) || (col == 3 && row == 11) || (col == 4 && row == 10)){
                    squares[col][row].PinkSquare();
                }
                if ((col == 10 && row == 4) || (col == 11 && row == 3) || (col == 12 && row == 2) || (col == 13 && row == 1)){
                    squares[col][row].PinkSquare();
                }
                if ((col == 10 && row == 10) || (col == 11 && row == 11) || (col == 12 && row == 12) || (col == 13 && row == 13)){
                    squares[col][row].PinkSquare();
                }
            }
        }


         //multiplier for letter/tile * 3
         //should be on:
         //B6, B10, F2, F6, F10, F14, J2, J6, J10, J14, N6, N10
        for (int row = 1; row < DIM ; row++){
            for (int col = 1; col < DIM ; col++){
                if ((col == 1 && row == 5) || (col == 1 && row == 9) || (col == 13 && row == 5) || (col == 13 && row == 9) ) {
                    squares[col][row].DarkBlueSquare();
                }
                if ((col == 5 && row == 1) || (col == 5 && row == 13) || (col == 5 && row == 5) || (col == 5 && row == 9) ||
                        (col == 9 && row == 1) || (col == 9 && row == 13) || (col == 9 && row == 5) || (col == 9 && row == 9)) {
                    squares[col][row].DarkBlueSquare();
                }
            }
        }


         //multiplier for letter/tile * 2
         //should be on:
         //A4, A12, C7, C9, D1, D8, D15, G3, G7, G9, G13, H4, H12, I3, I7, I9, I13, L1, L8, L15, M7, M9, O4, O12
        for (int row = 0; row < DIM ; row++){
            for (int col = 0; col < DIM ; col++){
                //A's and O's
                if ((col == 0 && row == 3) || (col == 0 && row == 11) || (col == 14 && row == 3) || (col == 14 && row == 11)) {
                    squares[col][row].BlueSquare();
                }
                //C's and M's
                if ((col == 2 && row == 6) || (col == 2 && row == 8) || (col == 12 && row == 6) || (col == 12 && row == 8)) {
                    squares[col][row].BlueSquare();
                }
                //D's and L's
                if ((col == 3 && row == 0) || (col == 3 && row == 7) || (col == 3 && row == 14) || (col == 11 && row == 0) || (col == 11 && row == 7) || (col == 11 && row == 14) ) {
                    squares[col][row].BlueSquare();
                }
                //G's and I's
                if ((col == 6 && row == 2) || (col == 6 && row == 6) || (col == 6 && row == 8) || (col == 6 && row == 12) || (col == 8 && row == 2) || (col == 8 && row == 6) || (col == 8 && row == 8) || (col == 8 && row == 12)  ) {
                    squares[col][row].BlueSquare();
                }
                //H's
                if ((col == 7 && row == 3) || (col == 7 && row == 11)) {
                    squares[col][row].BlueSquare();
                }
            }
        }
    }

    /**
     * changes inputted position, which is a string, to an actual position on the board
     * @requires position != null
     * @ensures result != null
     * @param position position inputted by player
     * @return actual position on the board
     */
    public int[] stringToPosition(String position) {
        position = position.toUpperCase();
        String pos = position.substring(1);

        int[] result = new int[2];
        result[1] = (int) (position.charAt(0)) - 'A';
        result[0] = Integer.parseInt(pos) - 1;
        String[] valStr = position.split("");
        return result;
    }


    /**
     * places tile on the board
     * @requires row != null && col != null && tile != null
     * @ensures Square != null
     * @param row row of the tile
     * @param col column of the tile
     * @param tile the chosen tile
     * @return the square which was chosen to set a tile on, with the newly added tile
     */
    public Square placeTile(int row, int col, Tiles tile){
        this.getSquare(row,col).setTileType(tile);
        this.getSquare(row, col).setAvailable();
        return this.getSquare(row, col);
    }

    /**
     * @requires row != null && col != null
     * @ensures Square != null
     * @param row chosen row
     * @param col chosen column
     * @return chosen square
     */
    public Square getSquare(int row, int col){
        if (row < 0 || row >= DIM || col < 0 || col >= DIM){
            return null;
        }
        return this.squares[col][row];
    }


    /**
     * @return a copy of the board
     * @ensures the result is a new object, hence not this object
     * @ensures the values of all squares/positions of the copy match the ones of this Board
     */
    public Board boardCopy(){
        Board copyOfBoard = new Board();
        for (int row = 0; row < DIM; row++) {
            for (int column = 0; column < DIM; column++) {
                copyOfBoard.squares[column][row] = this.squares[column][row];
            }
        }
        return copyOfBoard;
    }

    /**
     * @ensures all squares on the board are empty
     */
    public void reset(){
        for (int row = 0; row < squares.length; row++) {
            for (int column = 0; column < squares.length; column++) {
                squares[column][row] = new Square(1, null, null, ANSI.BLACK_BACKGROUND_BRIGHT);
            }
        }
    }

}
