package scrabble.exceptions;

public class InvalidDirectionException extends Exception{
    public InvalidDirectionException(){
        super("Invalid direction given. Type HOR or VER.");
    }
}
