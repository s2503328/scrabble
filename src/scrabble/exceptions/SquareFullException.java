package scrabble.exceptions;

public class SquareFullException extends Exception{
    public SquareFullException(){
        super("cannot place tile, Square is full");
    }
}
