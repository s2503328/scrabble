package scrabble.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scrabble.exceptions.ExitProgram;
import scrabble.newProtocol.Client;
import scrabble.newProtocol.Server;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerTest {
    private Client client1;
    private Client client2;
    private Server server;
    private ServerSocket serverSocket;

    @BeforeEach
    void setUp(){
        client1 = new Client("Arda");
        client2 = new Client("Linn");

        try {
            serverSocket = new ServerSocket(1234);
             server = new Server(serverSocket);
             new Thread(server).start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Thread(server).start();
    }
    @Test
    void clientMapSizeTest(){

            client1.createConnection();
            assertEquals(1,server.getController().getClientMap().size());
            client2.createConnection();
            assertEquals(2,server.getController().getClientMap().size());

    }
}
