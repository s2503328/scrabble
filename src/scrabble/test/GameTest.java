package scrabble.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scrabble.model.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class GameTest {

    private Board board;
    private Player player1;
    private Player player2;
    private Game game;
    private TilesBag tilesBag;

    @BeforeEach
    void setUp(){
        List<Player> players = new ArrayList<>();

        board = new Board();
        tilesBag = new TilesBag();
        player1 = new Player("A");
        player2 = new Player("B");
        players.add(player1);
        players.add(player2);
        game = new Game(players);
        player1.rack.add(Tiles.W);
        player1.rack.add(Tiles.R);
        player1.rack.add(Tiles.E);
        player1.rack.add(Tiles.C);
        player1.rack.add(Tiles.K);
        player2.rack.add(Tiles.B);
        player2.rack.add(Tiles.L);
        player2.rack.add(Tiles.U);
        player2.rack.add(Tiles.F);
        player2.rack.add(Tiles.F);
        game.giveTiles(player1);
        game.giveTiles(player2);

    }

    @Test
    void testTileCheck(){
        //check for board availability
        board.placeTile(7,7,Tiles.A);
        assertFalse(board.getSquare(7, 7).isAvailable());
        board.placeTile(7,7,null);
        assertTrue(board.getSquare(7,7).isAvailable());

        //check if player has a specific tile
        board.placeTile(3,4, player1.rack.get(2));
        assertSame(player1.rack.get(2), board.getSquare(3, 4).getTileType());
    }



    @Test
    void testCalculateScore() {
        assertEquals(16,game.calculateScore("taboo", 7,7,"HOR"));
        assertEquals(13,game.calculateScore("arch", 6,6,"VER"));

        //check that blank doesn't give points
        assertEquals(14,game.calculateScore("lov0e", 9,7,"HOR"));
    }


    @Test
    void testIsWinner(){
        player1.score = 0;
        player2.score = game.calculateScore("bluff","h8","hor");
        game.isWinner();

        assertEquals(player2, game.isWinner());

    }

    @Test
    void testGameOverEmptyBagAndRack(){
        game.getTilesBag().getTilesList().clear();
        player1.rack.clear();
        player2.rack.clear();
        assertTrue(game.gameOver());
    }

    @Test
    void testGameOverWithSkip(){
        player1.setSkip(true);
        player2.setSkip(true);
        assertTrue(game.gameOver());
    }



}