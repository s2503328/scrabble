package scrabble.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scrabble.model.Tiles;
import scrabble.model.TilesBag;

import static org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertNull;

class TilesBagTest {
    private TilesBag tilesBag;

   @BeforeEach
    void setUp(){
       tilesBag = new TilesBag();
   }


   @Test
    void testTileAmount(){
       assertEquals(100, tilesBag.tileAmount().size());
   }

   @Test
    void testTileTypes(){
       assertTrue(tilesBag.getTilesList().contains(Tiles.A));
       assertTrue(tilesBag.getTilesList().contains(Tiles.B));
       assertTrue(tilesBag.getTilesList().contains(Tiles.C));
       assertTrue(tilesBag.getTilesList().contains(Tiles.D));
       assertTrue(tilesBag.getTilesList().contains(Tiles.E));
       assertTrue(tilesBag.getTilesList().contains(Tiles.F));
       assertTrue(tilesBag.getTilesList().contains(Tiles.G));
       assertTrue(tilesBag.getTilesList().contains(Tiles.H));
       assertTrue(tilesBag.getTilesList().contains(Tiles.I));
       assertTrue(tilesBag.getTilesList().contains(Tiles.J));
       assertTrue(tilesBag.getTilesList().contains(Tiles.K));
       assertTrue(tilesBag.getTilesList().contains(Tiles.L));
       assertTrue(tilesBag.getTilesList().contains(Tiles.M));
       assertTrue(tilesBag.getTilesList().contains(Tiles.N));
       assertTrue(tilesBag.getTilesList().contains(Tiles.O));
       assertTrue(tilesBag.getTilesList().contains(Tiles.P));
       assertTrue(tilesBag.getTilesList().contains(Tiles.Q));
       assertTrue(tilesBag.getTilesList().contains(Tiles.R));
       assertTrue(tilesBag.getTilesList().contains(Tiles.S));
       assertTrue(tilesBag.getTilesList().contains(Tiles.T));
       assertTrue(tilesBag.getTilesList().contains(Tiles.U));
       assertTrue(tilesBag.getTilesList().contains(Tiles.V));
       assertTrue(tilesBag.getTilesList().contains(Tiles.W));
       assertTrue(tilesBag.getTilesList().contains(Tiles.X));
       assertTrue(tilesBag.getTilesList().contains(Tiles.Y));
       assertTrue(tilesBag.getTilesList().contains(Tiles.Z));
       assertTrue(tilesBag.getTilesList().contains(Tiles.BLANK));
   }
   @Test
    void testAftergettingTiles(){
       tilesBag.getRandomTile();
       assertEquals(99,tilesBag.getTilesList().size());
       tilesBag.getRandomTile();
       tilesBag.getRandomTile();
       tilesBag.getRandomTile();
       assertEquals(96,tilesBag.getTilesList().size());

    }
}