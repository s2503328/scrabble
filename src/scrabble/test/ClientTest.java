package scrabble.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scrabble.newProtocol.Client;
import scrabble.newProtocol.Server;


import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.Scanner;

public class ClientTest {
    private final static ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final static PrintStream originalOut = System.out;

    private Client client1;
    private Client client2;
    private Server server;
    private ServerSocket serverSocket;



    @BeforeEach
      void setUp(){
        client1 =  new Client("Arda");
        client2 =  new Client("Linn");
        try{
            serverSocket = new ServerSocket(1234);
        } catch (IOException e){
            e.printStackTrace();
        }
        server = new Server(serverSocket);
        new Thread(server).start();
        client1.createConnection();
    }

    @Test
    void joinMessageTest(){
        String msgFromServer = null;
        try {
            msgFromServer= client1.getIn().readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert msgFromServer != null;
       server.shutDown();

        assertTrue(msgFromServer.contains("Attempting to connect to localhost/127.0.0.1 :"
                + 1234 + "..."));

        client1.clearConnection();

    }


}
