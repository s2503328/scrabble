package scrabble.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scrabble.model.Board;
import scrabble.model.Tiles;
import scrabble.view.PrinterBoard;

import static org.junit.jupiter.api.Assertions.*;


class BoardTest {
    private Board board;
    private PrinterBoard pb;

    @BeforeEach
    public void setUp(){
        board = new Board();
        pb = new PrinterBoard();
        pb.setBoard(board);
    }

    @Test
    public void testBoardCopy() {
        board.placeTile(8, 6, Tiles.T);
        Board copy = board.boardCopy();
        copy.placeTile(8, 9, Tiles.E);

        assertEquals(Tiles.T, board.getSquare(8, 6).getTileType());
        assertEquals(Tiles.T, copy.getSquare(8, 6).getTileType());
        assertEquals(Tiles.E, copy.getSquare(8, 9).getTileType());

    }



	@Test
    public void testReset() {
        board.reset();
       for (int row = 0 ; row < 15 ; row++){
           for (int col = 0 ; col < 15 ; col++){
               assertNull(board.getSquare(col, row).getTileType());
           }
       }
    }

    @Test
    public void testPlaceTile(){
        board.placeTile(7, 7, Tiles.T);
        board.placeTile(7,8,Tiles.A);
        board.placeTile(7, 9, Tiles.B);
        board.placeTile(7, 10, Tiles.O);
        board.placeTile(7, 11, Tiles.O);
        System.out.println(pb.makeBoard());
        assertEquals(board.getSquare(7, 7).getTileType(), Tiles.T);
        assertEquals(board.getSquare(7, 8).getTileType(), Tiles.A);
        assertEquals(board.getSquare(7, 9).getTileType(), Tiles.B);
        assertEquals(board.getSquare(7, 10).getTileType(), Tiles.O);
        assertEquals(board.getSquare(7, 11).getTileType(), Tiles.O);


    }
}