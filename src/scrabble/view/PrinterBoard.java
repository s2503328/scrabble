package scrabble.view;

import scrabble.model.*;

import static scrabble.model.Board.DIM;

public class PrinterBoard {

    protected Board board;

    protected Game game;

    /**
     * @ensures game != null
     * @param game the current game
     */
    public void setGame(Game game) {
        this.game = game;
        this.board = game.getBoard();
    }


    /**
     * @ensures String != null
     * @return text
     */
    public String askForMove(){
        return "\n Enter your command. To make a move, please enter it as follows: \n MOVE;word;position;direction\n With direction either being 'HOR' or 'VER' \n Write 'HELP' to see the commands\n";
    }

    /**
     * @requires player != null
     * @ensures String != null
     * @return text
     */
    public String showPlayer(Player player){
        return "Player: " + player.getName() + ", Tiles: " + player.rack.toString() + ", Current Score: " + player.score;
    }

    /**
     * @requires player != null
     * @ensures String != null
     * @return text
     */
    public String scoreAfterMove(Player player){
        return "Player " + player.getName() + ", with that move, your total score is: " + player.score + "\n";
    }

    /**
     * @ensures String != null
     * @return text
     */
    public String invalidMove(){
        return "Sorry, that move is invalid. Please try again next turn.\n";
    }

    /**
     * @ensures String != null
     * @return text
     */
    public String invalidDir(){
        return "Sorry, that's not a valid direction. Please try 'HOR' for horizontal or 'VER' for vertical.\n";
    }

    /**
     * @ensures String != null
     * @return text
     */
    public String waitingTurn(){
        return "Your opponent is currently making a move. Please wait for your turn. While waiting, here is your rack and score: \n";
    }

    /**
     * @ensures String != null
     * @return text
     */
    public String helpMenu(){
        return "Your options are: \n MOVE ........ MOVE;WORD;POSITION;DIRECTION\n SWAP ........ SWAP;TILES\n SKIP ........ SKIP\n CHAT ........ CHAT;MESSAGE \n";
    }

    /**
     * @ensures String != null
     * @return text
     */
    public String skipMessage(){
        return "You have skipped your turn\n";
    }

    /**
     * @ensures String != null
     * @return text
     */
    public String gameOverMessage(Player winner){
        return "Game Over\nWinner is: " + winner.getName() + "! with score: " + winner.score;
    }

    /**
     * @ensures board != null
     * @param board the board for the game
     */
    public void setBoard(Board board) {
        this.board = board;
    }

    /**
     * @requires board != null
     * @ensures returned string != null
     *
     * @return String representation of the board
     */
    public  String makeBoard(){

        StringBuilder sb = new StringBuilder();

        sb.append("    ");

        //generate letters for each column
        for (int i = 0; i < DIM ; i++){
            sb.append("   " + (char)(65+i) + "  ");

        }

        //board squares actually starting
        sb.append("\n");
        //starting corner top row
        sb.append("    ┌");

        //top part of first row
        for (int horizontal = 0; horizontal < DIM -1; horizontal++){
            sb.append("─────┬");
        }

        //end of top part of the first row
        sb.append("─────┐");
        sb.append("\n");

        for (int vertical = 0; vertical < DIM; vertical++){
            //printing out the numbers for each row
            if (vertical < 9){
                sb.append( " " + (vertical+1) + "  ");
            }
            else{
                sb.append(" " + (vertical +1) +" ");
            }
            sb.append("│");
            for (int horizontal = 0; horizontal < DIM; horizontal++){
                Square square = board.getSquare(vertical, horizontal);

                //printing out the colored squares/multipliers
                switch (square.getMultiplier()){
                    //for the non-multipliers on the board
                    case 1:
                        sb.append(ANSI.BLACK_BACKGROUND);
                        break;
                    case 2:
                        //for word *2 and for middle square of the board
                        if (square.getMultiplierType().equals("WORD")){
                            sb.append(ANSI.PURPLE_BACKGROUND_BRIGHT);
                        }
                        //for letter*2
                        else{
                            sb.append(ANSI.CYAN_BACKGROUND_BRIGHT);
                        }
                        break;
                    case 3:
                        //for word *3
                        if (square.getMultiplierType().equals("WORD")){
                            sb.append(ANSI.RED_BACKGROUND);
                        }
                        //for letter *3
                        else{
                            sb.append(ANSI.BLUE_BACKGROUND);
                        }
                        break;
                    default:
                }


                //prints out placed tiles / replaces color with tile
                Tiles tile = square.getTileType();

                if (tile != null){
                    sb.append(ANSI.RESET);
                    if (tile == Tiles.BLANK) {
                        tile = square.getBlankLetter();
                    }
                    sb.append("  " + tile.toString() + "  ");
                }
                else {
                    sb.append("     ");
                }
                sb.append(ANSI.RESET);
                //end of square
                sb.append("│");

            }
            //add numbers on the other side of the board
            if (vertical < 9){
                sb.append(ANSI.RESET);
                sb.append( "  " + (vertical+1) + "  ");
            }
            else {
                sb.append(ANSI.RESET);
                sb.append(" " + (vertical + 1) + " ");
            }
            //end of row
            sb.append("\n    ");


            //if there are more rows
            if (vertical < DIM -1){
                sb.append("├");
            }
            //if it is the last row
            else {
                sb.append("└");
                sb.append(ANSI.RESET);

            }

            //make the bottom of each square per row/column
            for (int horizontal = 0; horizontal < DIM; horizontal++) {
                //if it is the last row and last column
                if ((vertical == DIM -1) && (horizontal == DIM -1)){
                    sb.append("─────┘");
                    sb.append(ANSI.RESET);
                }
                //if it is the last row but not last column
                else if ((vertical == DIM -1)&&(horizontal < DIM -1)){
                    sb.append("─────┴");
                    sb.append(ANSI.RESET);
                }
                //if it is not the last row, but it is the last column
                else if ((vertical < DIM -1)&&(horizontal == DIM -1)) {
                    sb.append("─────┤");
                    sb.append(ANSI.RESET);
                }
                //not the last row, not the last column
                else {
                    sb.append("─────┼");
                }
                sb.append(ANSI.RESET);
            }

            sb.append("\n");


        }

        sb.append("    ");
        //generate letters for each column
        for (int i = 0; i < DIM ; i++){
            sb.append("   " + (char)(65+i) + "  ");
        }
        return sb.toString() ;

    }




}
